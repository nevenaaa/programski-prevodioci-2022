%{
  #include <stdio.h>
  #include <stdlib.h>
  #include <string.h>
  #include <math.h>
  #include "defs.h"
  #include "symtab.h"

  int yyparse(void);
  int yylex(void);
  int yyerror(char *s);
  void warning(char *s);

  extern int yylineno;
  char char_buffer[CHAR_BUFFER_LENGTH];
  int error_count = 0;
  int warning_count = 0;
  int var_num = 0;
  int fun_idx = -1;
  int fcall_idx = -1;

  int stack_array[100];
%}

%union {
  int i;
  char *s;
}

%token <i> _TYPE
%token _IF
%token _ELSE
%token _RETURN
%token <s> _ID
%token <s> _INT_NUMBER
%token <s> _UINT_NUMBER
%token _LPAREN
%token _RPAREN
%token _LBRACKET
%token _RBRACKET
%token _ASSIGN
%token _SEMICOLON
%token <i> _AROP
%token <i> _RELOP
%token _POP
%token _PUSH
%token _TOP
%token _ISEMPTY
%token _STACK
%token _POINT
%token _LSQBRACKET
%token _RSQBRACKET
%token _PRIORITYQUEUE
%token _INSERT
%token _DEQUEUE
%token _PEEK
%token _COMMA
%token _EMPTY

%type <i> num_exp exp literal function_call argument rel_exp start_statement end_stack_statement push_statement pop_statement top_statement isEmpty_statement end_priority_queue_statement empty_statement insert_statement dequeue_statement peek_statement

%nonassoc ONLY_IF
%nonassoc _ELSE

%%

program
  : function_list
      {  
        if(lookup_symbol("main", FUN) == NO_INDEX)
          err("undefined reference to 'main'");
       }
  ;

function_list
  : function
  | function_list function
  ;

function
  : _TYPE _ID
      {
        fun_idx = lookup_symbol($2, FUN);
        if(fun_idx == NO_INDEX)
          fun_idx = insert_symbol($2, FUN, $1, NO_ATR, NO_ATR);
        else 
          err("redefinition of function '%s'", $2);
      }
    _LPAREN parameter _RPAREN body
      {
        clear_symbols(fun_idx + 1);
        var_num = 0;
      }
  ;

parameter
  : /* empty */
      { set_atr1(fun_idx, 0); }

  | _TYPE _ID
      {
        insert_symbol($2, PAR, $1, 1, NO_ATR);
        set_atr1(fun_idx, 1);
        set_atr2(fun_idx, $1);
      }
  ;

body
  : _LBRACKET variable_list statement_list _RBRACKET
  ;

variable_list
  : /* empty */
  | variable_list variable
  ;

variable
  : _TYPE _ID _SEMICOLON
      {
        if(lookup_symbol($2, VAR|PAR) == NO_INDEX)
           insert_symbol($2, VAR, $1, ++var_num, NO_ATR);
        else 
           err("redefinition of '%s'", $2);
      }
  ;

statement_list
  : /* empty */
  | statement_list statement
  ;

statement
  : compound_statement
  | assignment_statement
  | if_statement
  | return_statement
  | stack_statement
  | function_stack_statement
  | priority_queue_statement
  | function_priority_queue_statement
  ;

compound_statement
  : _LBRACKET statement_list _RBRACKET
  ;

assignment_statement
  : _ID _ASSIGN num_exp _SEMICOLON
      {
        int idx = lookup_symbol($1, VAR|PAR);
        if(idx == NO_INDEX)
          err("invalid lvalue '%s' in assignment", $1);
        else
          if(get_type(idx) != get_type($3))
            err("incompatible types in assignment");
      }
  ;

num_exp
  : exp
  | num_exp _AROP exp
      {
        if(get_type($1) != get_type($3))
          err("invalid operands: arithmetic operation");
      }
  ;

exp
  : literal
  | _ID
      {
        $$ = lookup_symbol($1, VAR|PAR);
        if($$ == NO_INDEX)
          err("'%s' undeclared", $1);
      }
  | function_call
  | _LPAREN num_exp _RPAREN
      { $$ = $2; }
  ;

literal
  : _INT_NUMBER
      { $$ = insert_literal($1, INT); }

  | _UINT_NUMBER
      { $$ = insert_literal($1, UINT); }
  ;

function_call
  : _ID 
      {
        fcall_idx = lookup_symbol($1, FUN);
        if(fcall_idx == NO_INDEX)
          err("'%s' is not a function", $1);
      }
    _LPAREN argument _RPAREN
      {
        if(get_atr1(fcall_idx) != $4)
          err("wrong number of args to function '%s'", 
              get_name(fcall_idx));
        set_type(FUN_REG, get_type(fcall_idx));
        $$ = FUN_REG;
      }
  ;

argument
  : /* empty */
    { $$ = 0; }

  | num_exp
    { 
      if(get_atr2(fcall_idx) != get_type($1))
        err("incompatible type for argument in '%s'",
            get_name(fcall_idx));
      $$ = 1;
    }
  ;

if_statement
  : if_part %prec ONLY_IF
  | if_part _ELSE statement
  ;

if_part
  : _IF _LPAREN rel_exp _RPAREN statement
  ;

rel_exp
  : num_exp _RELOP num_exp
      {
        if(get_type($1) != get_type($3))
          err("invalid operands: relational operator");
      }
  ;

return_statement
  : _RETURN num_exp _SEMICOLON
      {
        if(get_type(fun_idx) != get_type($2))
          err("incompatible types in return");
      }
  ;

stack_statement
  : _STACK _LSQBRACKET _TYPE _RSQBRACKET  _ID
	{ /*provera da li je promenljiva prethodno deklarisana i njena dekaracija*/
	   int stackElement = 5;
	   int indx = lookup_symbol($5, (STA));
    	   if(indx == NO_INDEX)
	      	insert_symbol($5, STA, $3, stackElement, 0); 
	   else
	  	err("Stek '%s' je prethodno vec deklarisan!", $5);
	   for(int i = 0; i<stackElement; i++){
		char *str = malloc(strlen($5)+1);
		char ch = i+'0';
		strcpy(str, $5);
		strncat(str, &ch, 1);
	      	insert_symbol(str, ELM, $3, 0, 0);
	   }
	   //print_symtab(); 
	}
    _ASSIGN _STACK _LSQBRACKET _TYPE
	{/*tipovi moraju da se poklapaju*/
	   if($3 != $10)
		err("Tipovi prilikom inicijalizacije steka moraju da se poklapaju!");
	}
    _RSQBRACKET _LPAREN _RPAREN _SEMICOLON
  ;

function_stack_statement
 : start_statement end_stack_statement
	{ //$1 - indeks steka
	   if($2 != -1 && $2 != -2 && $2 != -3 && get_type($1) != get_type($2))
              err("Tip elementa koji se stavlja na stek mora biti istog tipa kao i stek!\n");
	   else if($2 != -1 && $2 != -2 && $2 != -3){

		unsigned numberOfElement = get_atr2($1);//broj elemenata na steku
		if(numberOfElement >= 5)
		  err("Stek je pun, nije moguce ubacivanje elemenata vise.\n");
		else{
			for(int i = 0; i<numberOfElement; i++){//prebacivanje elemenata na dole
				int elementUp = $1+i+1;
				unsigned value = get_atr1(elementUp);
				int elementDown = $1+i+2;
				set_atr1(elementDown, value);
			}
			int element = $1+1;
			set_atr1(element, $2);//element novi
			unsigned incrementNumber = numberOfElement + 1;
			set_atr2($1, incrementNumber);//stek
			print_symtab();
		}

           }else if($2 == -1){

		
		unsigned numberOfElement = get_atr2($1);
		if(numberOfElement == 0)
		  	err("Stek je prazan nije moguce nista skinuti sa njega.\n");
		else{
			int element = $1+1;
			set_atr1(element, 0);//element koji se skida
			for(int i = 0; i<numberOfElement; i++){ //prebacivanje elemenata na gore
				int elementUp = $1+i+1;
				int elementDown = $1+i+2;
				unsigned value = get_atr1(elementDown);
				set_atr1(elementUp, value);
			}
			unsigned decrementNumber = numberOfElement - 1;
			set_atr2($1, decrementNumber);//stek
		}
	      	print_symtab();
 	

	   }else if($2 == -2){

		unsigned numberOfElement = get_atr2($1);
		if(numberOfElement == 0)
		      err("Stek je prazan nije moguce videti poslednji njegov element. \n");
		else{
		      int element = $1+1;
		      int indexLit = get_atr1(element);
		      char * name = get_name(indexLit);
		      printf("Na vrhu steka je %s \n",name);
		}
 		print_symtab();
	   }else if($2 == -3){
		unsigned numberOfElement = get_atr2($1);
		if(numberOfElement == 0) 
		   printf("Stek je prazan \n");
		else
		   printf("Stek nije prazan \n");
	   }
	
	}
 ;

start_statement
  : _ID 
	{/*provera da li je stek/red sa prioritetom deklarisan*/
	  int indx = lookup_symbol($1, (STA|PQU)); 
    	  if(indx == NO_INDEX)
	     err("Struktura '%s' nije deklarisana", $1);  
	}
    _POINT
	{ $$ = lookup_symbol($1, (STA|PQU)); }
  ;

end_stack_statement
 : push_statement { $$ = $1; }
 | pop_statement { $$ = $1; }
 | top_statement { $$ = $1; }
 | isEmpty_statement { $$ = $1; }
 ;

push_statement
  : _PUSH _LPAREN literal _RPAREN _SEMICOLON { $$ = $3; }
  ;

pop_statement
  : _POP _LPAREN _RPAREN _SEMICOLON { $$ = -1; }	   
  ;

top_statement
  : _TOP _LPAREN _RPAREN _SEMICOLON { $$ = -2; }
  ;

isEmpty_statement
  : _ISEMPTY _LPAREN _RPAREN _SEMICOLON {$$ = -3; }
  ;

priority_queue_statement
  : _PRIORITYQUEUE _LSQBRACKET _TYPE _RSQBRACKET _ID
	{ /*provera da li je red sa prioritetom prethodno deklarisan i njegova dekaracija*/
	   int queueElement = 5;
	   int indx = lookup_symbol($5, (PQU));
    	   if(indx == NO_INDEX)
	      	insert_symbol($5, PQU, $3, queueElement, 0); 
	   else
	  	err("Red sa prioritetom '%s' je prethodno vec deklarisan!", $5);
	   for(int i = 0; i<queueElement; i++){
		char *str = malloc(strlen($5)+1);
		char ch = i+'0';
		strcpy(str, $5);
		strncat(str, &ch, 1);
	      	insert_symbol(str, ELM, $3, 0, 0);
            }
	}
    _ASSIGN _PRIORITYQUEUE _LSQBRACKET _TYPE
	{/*tipovi moraju da se poklapaju*/
	   if($3 != $10)
		err("Tipovi prilikom inicijalizacije reda sa prioritetom moraju da se poklapaju!");
	}
    _RSQBRACKET _LPAREN _RPAREN _SEMICOLON
  ;

function_priority_queue_statement
  : start_statement end_priority_queue_statement
	{ //$1 - indeks reda sa priritetom

	   if($2 != -1 && $2 != -2 && $2 != -3)
		 //printf("PUSH"); 

		//trenutni broj elemenata u redu
		unsigned numberOfElement = get_atr2($1);
		if(numberOfElement >= 5)
		  err("Red sa prioritetom je pun, nije moguce ubacivanje elemenata vise.\n");
		//indeks elementa za dodavanje
		int element = $1+numberOfElement+1;
	 	unsigned priority = get_atr2($2);
		set_atr1(element, $2);//element novi - indeks literala
		set_atr2(element, priority); //element novi - prioritet
		unsigned incrementNumber = numberOfElement + 1;
		set_atr2($1, incrementNumber);//stek - inkrement broja elemenata
		
		//lista prioriteta - nesortirana
		unsigned listForSort[5];
		unsigned listOfElements[5];
		for(int i=0; i<5;i++){
			int element = $1+i+1;
			unsigned currentPriority = get_atr2(element);
			listForSort[i] = currentPriority;
			
			unsigned currentElement = get_atr1(element);
			listOfElements[i] = currentElement;
		}
		
		//sortiranje liste prioriteta
		int tmp;
		for(int i=0; i<5; i++)
		    {
			for(int j=i+1; j<5; j++)
			{
			    if(listForSort[j] > listForSort[i])
			    {
				tmp = listForSort[i];
				listForSort[i] = listForSort[j];
				listForSort[j] = tmp;
			    }
			}
		    }
		//ubacivanje elemenata po prioritetu
		for(int i=0; i<5; i++)
		{
			for(int j=0; j<5; j++)
			{
				unsigned currentElement = listOfElements[j];
				unsigned currentPriority = get_atr2(currentElement);
				if(currentPriority==listForSort[i]){
					//indeks elementa za dodavanje
					int element = $1+i+1;
					set_atr1(element, currentElement);
					set_atr2(element, currentPriority); 
				}
				
			}
			
		}
		
		

	      
	      //print_symtab();
           }else if($2 == -1){

		 //printf("DEQUEUE");
		unsigned numberOfElement = get_atr2($1);
		if(numberOfElement == 0)
		  err("Red sa prioritetom je prazan nije moguce nista skinuti sa njega.\n");
		else{
			int element = $1+1;
			set_atr1(element, 0);//element koji se skida
			set_atr2(element, 0);
			unsigned decrementNumber = numberOfElement - 1;
			set_atr2($1, decrementNumber);//broj elemenata u redu sa prioritetom

			//povlacenje elemenata na gore
			for(int i =0; i<decrementNumber; i++){
				int elementUp = $1+1+i;
				int elemenDown = $1+2+i;
				unsigned atr1 = get_atr1(elemenDown);
				unsigned atr2 = get_atr2(elemenDown);
				set_atr1(elementUp, atr1);
				set_atr2(elementUp, atr2);
			}
			int elementLast = $1+1+decrementNumber;
			set_atr1(elementLast, 0);
			set_atr2(elementLast, 0);

		}
		
		

		//print_symtab();

	   }else if($2 == -2){

		 //printf("PEEK");
 	      unsigned numberOfElement = get_atr2($1);
	      if(numberOfElement == 0)
		      err("Red sa prioritetom je prazan nije moguce videti prvi njegov element. \n");
	      else{
		      int element = $1+1;
		      int indexLit = get_atr1(element);
		      char * name = get_name(indexLit);
		      unsigned priority = get_atr2(indexLit);
		      printf("Prvi element je %s sa prioritetom %d \n",  name, priority);
	      }

	   }else if($2 == -3){//empty
	      unsigned numberOfElement = get_atr2($1);
	      if(numberOfElement == 0) 
		   printf("Red sa prioritetom je prazan \n");
	      else
		   printf("Red sa prioritetom nije prazan \n");
	   }
	
	}
  ;

end_priority_queue_statement
  : empty_statement { $$ = $1; }
  | insert_statement { $$ = $1; }
  | dequeue_statement { $$ = $1; }
  | peek_statement { $$ = $1; }
  ; 

insert_statement
  : _INSERT _LPAREN literal _COMMA literal _RPAREN _SEMICOLON 
/*literali moraju biti istog tipa */  {
	     unsigned priority = atoi(get_name($5));
	     if(get_atr2($3) != 0)
		err("Nije moguce staviti isti element sa razlicitim prioritetom!");
	     else{
		set_atr2($3, priority);
		$$ = $3;
	     }
	}
  ;

peek_statement /*vraca prvi element reda*/
  : _PEEK _LPAREN _RPAREN _SEMICOLON {$$ = -2; }
  ;

dequeue_statement /*skida element iz reda*/
  : _DEQUEUE _LPAREN _RPAREN _SEMICOLON {$$ = -1; }
  ;

empty_statement
  : _EMPTY _LPAREN _RPAREN _SEMICOLON {$$ = -3; }
  ;

%%

int yyerror(char *s) {
  fprintf(stderr, "\nline %d: ERROR: %s", yylineno, s);
  error_count++;
  return 0;
}

void warning(char *s) {
  fprintf(stderr, "\nline %d: WARNING: %s", yylineno, s);
  warning_count++;
}

int main() {
  int synerr;
  init_symtab();

  synerr = yyparse();

  clear_symtab();
  
  if(warning_count)
    printf("\n%d warning(s).\n", warning_count);

  if(error_count)
    printf("\n%d error(s).\n", error_count);

  if(synerr)
    return -1; //syntax error
  else
    return error_count; //semantic errors
}

