//OPIS: stack, PriorityQueue
int main() {
    Stack[int] s = Stack[int]();
    s.push(3);
    s.push(8);
    s.pop();
    s.top();
    s.isEmpty();
   

    PriorityQueue[int] p = PriorityQueue[int]();
    p.insert(5,5);
    p.insert(3,8);
    p.dequeue();
    p.peek();
    p.empty();
}
