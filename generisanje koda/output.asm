
main:
		PUSH	%14
		MOV 	%15,%14
		SUBS	%15,$16,%15
@main_body:
		MOV 	$6,-4(%14)
		MOV 	$2,-12(%14)
		MOV 	$2,-4(%14)
		MOV 	$6,-8(%14)
		MOV 	$1,-16(%14)
		MOV 	-4(%14),%13
		JMP 	@main_exit
@main_exit:
		MOV 	%14,%15
		POP 	%14
		RET