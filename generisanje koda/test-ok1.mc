//OPIS: inicijalizacija steka i operacije sa stekom
//RETURN: 6
int main() {
    int x;
    int a;
    int c;
    int d;
    
    Stack[int] s = Stack[int]();
    x = s.push(6);
    c = s.push(2);
    x = s.pop();
    a = s.top();
    d = s.isEmpty();
    return x;
}


