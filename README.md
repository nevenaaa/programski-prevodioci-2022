# Programski prevodioci 2022 

## Osnovni podaci
Nevena Prokić SW6/2019

## Tema
Stack - sintaksa, semantika, generisanje koda (push, pop, top, isEmpty) <br/>
Priority Queue - sintaksa i semantika (insert, dequeue, peek, empty) <br/>

## Način pokretanja
Oba projekta se pokreću kao što je na vežbama rađeno sa _make_ i _make test_. 



